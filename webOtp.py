from flask import Flask, render_template, request
import otp

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index2.html')

@app.route('/usuarios')
def usuarios():
    usuarios = otp.get_usuarios()
    
    return render_template('usuarios.html', usuarios=usuarios)

@app.route('/login')
def login():
    return render_template('frmlogin.html')

@app.route('/validaLogin', methods=['post'])
def valida_login():
    message = ''
    if request.method == 'POST':
       email = request.form.get('email')
       senha = request.form.get('senha')
    else:
       message = 'Falha na requisição de login!'
    
    contas=[]
    if otp.login(email,senha):
       contas = otp.get_contas_email(email)
       message = 'Login efetuado com sucesso!'
       return render_template('usuario.html', usuario=email, contas=contas, message=message)
    else:
        message = 'Falha no login!'
    
    return render_template('usuario.html', usuario=email, contas=contas, message=message)

@app.route('/registro')
def registro():
    return render_template('frmregistro.html')

@app.route('/validaregistro', methods=['post'])
def valida_registro():
    message = ''
    if request.method == 'POST':
        email = request.form.get('email')
        senha = request.form.get('senha')
    else:
        message = 'Falha na requisição de login!'

    if otp.add_user(email,senha):
        message = 'Registro efetuado com sucesso!'
    else:
        message = 'Falha no Registro, email já foi utilizado para cadastro!'
    
    return render_template('resultado.html', message=message)

@app.route('/contas', methods=['post'])
def contas():
    message = ''
    if request.method == 'POST':
        oid_usuario = request.form.get('oid_usuario')
        contas = request.form.get('contas')
        usuario = request.form.get('usuario')
    else:
        message = 'Operação não suportada!'

    contas = otp.get_contas(oid_usuario)

    return render_template('usuario.html', usuario=usuario, contas=contas)

@app.route('/conta', methods=['post'])
def frmconta():
    if request.method == 'POST':
        usuario = request.form.get('usuario')
    return render_template('frmconta.html', usuario=usuario)

@app.route('/validaconta', methods=['post'])
def valida_conta():
    message = ''
    if request.method == 'POST':
      usuario = request.form.get('usuario')
      conta = request.form.get('conta')
    else:
      message = 'Falha na geração de chave OTP!'
      return render_template('resultado.html', usuario='', conta='', message=message)

    res, message = otp.add_nova_conta(conta, usuario)
    
    return render_template('resultado.html', usuario=usuario, conta=conta, message=message)

@app.route('/exibe_chave', methods=['POST'])
def exibe_chave():
    message = ''
    if request.method == 'POST':
        oid_conta = request.form.get('oid_conta')
        usuario = request.form.get('usuario')
        conta = request.form.get('conta')
    else:
        return render_template('resultado.html',message = 'Falha na recuperação da chave OTP!')

    secret = otp.get_secret_conta(oid_conta)

    return render_template('exibe_secret.html', oid_conta=oid_conta, conta=conta, usuario=usuario, secret=secret, message=message)

@app.route('/chave', methods=['post'])
def frmchave():
    if request.method == 'POST':
        oid_conta = request.form.get('oid_conta')
        usuario = request.form.get('usuario')
        secret = request.form.get('secret')

    conta = otp.get_nome_conta(oid_conta)

    return render_template('frmchave.html', usuario=usuario, conta=conta, secret=secret, oid_conta=oid_conta)

@app.route('/validachave', methods=['POST'])
def valida_chave():
    message = ''
    if request.method == 'POST':
        oid_conta = request.form.get('oid_conta')
        usuario = request.form.get('usuario')
        secret = request.form.get('secret')
    else:
        return render_template('resultado.html',message = 'Falha na recuperação da chave OTP!')

    if otp.get_valida_secret(oid_conta, secret):
        message="Secret validado com sucesso!!!"
    else:
        message="Secret inválido!!!"

    conta  = otp.get_nome_conta(oid_conta)

    return render_template('resultado.html', conta=conta, usuario=usuario, secret=secret, message=message)

@app.route('/qrcode', methods=['post'])
def qrcode():
    if request.method == 'POST':
        usuario = request.form.get('usuario')
        arquivo = request.form.get('arquivo')
    return render_template('qrcode.html', usuario=usuario, arquivo=arquivo)

