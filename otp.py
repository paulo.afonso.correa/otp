#
#
import pyotp
import time
import db_otp
import qrcode

def add_user(email, senha):
   if db_otp.existe_usuario(email):
      return False

   # grava usuario no banco
   db_otp.add_usuario(email,senha)
   oid = db_otp.get_oid_usuario(email)

   return True

def get_usuario(email):
   return db_otp.get_usuario(email)

def add_nova_conta(conta, email):
   oid_usuario = db_otp.get_oid_usuario(email)
   if db_otp.existe_conta(conta, oid_usuario):
     return False, 'Conta já existe com esse nome!!!'

   chave_otp = get_key()
   uri = get_uri(chave_otp, email)
   arquivo_qrcode = email + '_' + conta + '.png'

   # grava conta no banco
   db_otp.add_conta(conta, chave_otp, arquivo_qrcode, oid_usuario)
   
   save_qrcode(arquivo_qrcode, uri)

   return True, 'Conta criada com sucesso!!!'

def get_usuarios():
   usuarios = db_otp.get_usuarios()
   return list(usuarios)
   
def get_contas(oid_usuario):
   contas = db_otp.get_contas(oid_usuario)
   
   return list(contas)

def get_contas_email(email):

   contas = db_otp.get_contas(db_otp.get_oid_usuario(email))
   
   return list(contas)

def get_nome_conta(oid_conta):
   return db_otp.get_nome_conta(oid_conta)
   
def get_secret_conta(oid_conta):
   chave_otp = db_otp.get_chave_otp(oid_conta)
   
   return get_secret(chave_otp)
   
def get_valida_secret(oid_conta, secret):
   chave_otp = db_otp.get_chave_otp(oid_conta)

   return secret == get_secret(chave_otp)
   
#
def login(email, senha):
   if not db_otp.existe_usuario(email):
      return False

   user = get_usuario(email)

   #print(str(user[1]) + ' - ' + str(user[2]))
   
   # verifica senha
   #if user[2] != senha:
   #   return False

   return True
  
# funcoes OTP
#
def get_key():
    return pyotp.random_base32()
  
def get_secret(chave_otp):
    totp = pyotp.TOTP(chave_otp)
    return totp.now() 

def check_key(key):
    totp.verify(key)

def get_uri(chave_otp, email):
    return pyotp.TOTP(chave_otp).provisioning_uri(name=email, issuer_name='Secure App')

def save_qrcode(arquivo, string):
    img = qrcode.make(string)
  #  type(img)  # qrcode.image.pil.PilImage
    img.save("static/" + arquivo )


#####################################################
###
###   codigo extra
###
#####################################################
def atualiza_arquivos_qrcode():
   for u in get_usuarios():
      contas = get_contas(u[0])
      for c in contas:
        update_arquivo_qrcode_conta(c[0], c[1], c[2], u[1])
        #print(str(c[0]) + ' - ' + c[1] + ' - ' + c[2] + ' - ' + u[1]) 

def update_arquivo_qrcode_conta(oid_conta, conta, chave_otp, email):
   uri = get_uri(chave_otp, email)
   arquivo_qrcode = email + '_' + conta + '.png'
   save_qrcode(arquivo_qrcode, uri)

   ret = db_otp.update_arquivo_conta(oid_conta, arquivo_qrcode)
   
   return

