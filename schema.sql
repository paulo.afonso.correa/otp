--DROP TABLE usuario;

CREATE TABLE usuario (
    oid INTEGER PRIMARY KEY AUTOINCREMENT,
    email TEXT NOT NULL,
    senha TEXT NOT NULL
);

--DROP TABLE conta;

CREATE TABLE conta (
    oid INTEGER PRIMARY KEY AUTOINCREMENT,
    nome TEXT NOT NULL,
    chave_otp TEXT NOT NULL,
    oid_usuario INTEGER
);
