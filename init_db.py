import sqlite3

connection = sqlite3.connect('database.db')


with open('schema.sql') as f:
    connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO usuario (email, senha) VALUES (?, ?)",
            ('Paulo', 'f654FLH@1Ms8a')
            )
cur.execute("INSERT INTO usuario (email, senha) VALUES (?, ?)",
            ('Juarez', 'hjgfdgfdgrewewq$%118a')
            )

cur.execute("INSERT INTO conta (nome, chave_otp, oid_usuario) VALUES (?, ?, ?)",
            ('conta1', 'E25Z5JB3HHKQFDX62X6F2PM4GGHV7W4Z',1)
            )
cur.execute("INSERT INTO conta (nome, chave_otp, oid_usuario) VALUES (?, ?, ?)",
            ('conta1', 'MTIWUMRP6ZUAQZ7SOARFWJSSMFEG6MGI',2)
            )

connection.commit()
connection.close()
