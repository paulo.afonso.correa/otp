import sqlite3

def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn

def get_usuarios():
    conn = get_db_connection()
    usuarios = conn.execute('select oid, email  from usuario').fetchall()
    conn.close()
    return usuarios

def existe_usuario(email):
    result = True
    conn = get_db_connection()
    cur = conn.cursor()
    res = cur.execute('select oid from usuario where email = ?', [email])
    
    if res.fetchone() is None:
      result = False

    conn.close()
    return result

def get_oid_usuario(email):
    conn = get_db_connection()
    cur = conn.cursor()
    usuario = cur.execute('select oid from usuario where email = ?', [email]).fetchone()
    
    conn.close()
    return usuario[0]

def get_usuario(email):
    conn = get_db_connection()
    cur = conn.cursor()
    usuario = cur.execute('select * from usuario where email = ?', [email]).fetchone()
    
    if usuario is None:
      result = False

    conn.close()
    return list(usuario)

def update_arquivo_conta(oid_conta, arquivo_qrcode):
    conn = get_db_connection()
    cur = conn.cursor()
    res = cur.execute('update conta set arquivo_qrcode = ? where oid = ?', [str(arquivo_qrcode), str(oid_conta)])
    print(res)
    conn.commit()
    conn.close()
    return res

def get_contas(oid_usuario):
    conn = get_db_connection()
    cur = conn.cursor()
    contas = cur.execute('select oid, nome, arquivo_qrcode  from conta where oid_usuario = ?', [str(oid_usuario)]).fetchall()
    conn.close()
    return contas

def existe_conta(nome,oid_usuario):
    result = True
    conn = get_db_connection()
    cur = conn.cursor()
    res = cur.execute('select oid from conta where nome = ? and oid_usuario = ?', [str(nome), str(oid_usuario)])

    if res.fetchone() is None:
      result = False

    conn.close()
    return result

def get_nome_conta(oid_conta):
    conn = get_db_connection()
    cur = conn.cursor()
    print("OID_CONTA: "+oid_conta)
    nome = cur.execute('select nome from conta where oid = ?', [str(oid_conta)]).fetchall()
    conn.close()
    return nome[0][0]

def get_chave_otp(oid_conta):
    conn = get_db_connection()
    cur = conn.cursor()
    chave = cur.execute('select chave_otp from conta where oid = ?', [str(oid_conta)]).fetchall()
    conn.close()
    return chave[0][0]

def add_usuario(email, senha):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("INSERT INTO usuario (email, senha) VALUES (?, ?)",
            (email, senha))
    conn.commit()
    conn.close()

def add_conta(nome, chave_otp, arquivo_qrcode, oid_usuario):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("INSERT INTO conta (nome, chave_otp, arquivo_qrcode, oid_usuario) VALUES (?, ?, ?, ?)",
            (nome, chave_otp, arquivo_qrcode, oid_usuario))
    conn.commit()
    conn.close()

